package cl.mobdev.trainee.rickandmorty.domain.usecase;

import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import cl.mobdev.trainee.rickandmorty.domain.port.output.IGetCharacterByIDGateway;
import cl.mobdev.trainee.rickandmorty.domain.port.output.IGetOriginByURLGateway;
import cl.mobdev.trainee.rickandmorty.domain.usecase.GetCharacterByIDUseCase;
import cl.mobdev.trainee.rickandmorty.mocks.CharacterMocks;
import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GetCharacterByIDUseCaseTest {/*
    @Mock
    private IGetCharacterByIDGateway getCharacterByID;
    @Mock
    private IGetOriginByURLGateway getOriginByURLGateway;
    private GetCharacterByIDUseCase getCharacterByIDUseCase;

    @BeforeEach
    void setUp(){
        getCharacterByIDUseCase=new GetCharacterByIDUseCase(getCharacterByID, getOriginByURLGateway);
    }


    @Test
    void should_return_character_complete(){
        Character expectedCharacter=new Character();
        expectedCharacter.setName("Rick");
        expectedCharacter.setOrigin(new Origin());
        expectedCharacter.getOrigin().setName("Earth");

        //GIVEN
        int id=1;
        String url="www.earth.cl";

        Character character= CharacterMocks.characterComplete();

        Origin origin=new Origin();
        origin.setName("Earth");
        origin.setUrl("www.earth.cl");

        Mockito.when(getCharacterByID.execute(id)).thenReturn(character);
        Mockito.when(getOriginByURLGateway.execute(url)).thenReturn(origin);

        //WHEN
        Character resultCharacter=getCharacterByIDUseCase.execute(id);

        //THEN
        assertEquals(expectedCharacter.getName(), resultCharacter.getName());
        assertEquals(expectedCharacter.getOrigin().getName(), resultCharacter.getOrigin().getName());
    }

    @Test
    void should_return_character_when_origin_null(){
        Character expectedCharacter=new Character();
        expectedCharacter.setName("Rick");
        expectedCharacter.setOrigin(new Origin());
        expectedCharacter.getOrigin().setName("unknown");

        //GIVEN
        int id=1;
        Character character=CharacterMocks.characterWithOriginNull();

        when(getCharacterByID.execute(id)).thenReturn(character);

        //WHEN
        Character resultCharacter=getCharacterByIDUseCase.execute(id);

        //THEN
        assertEquals(expectedCharacter.getName(), resultCharacter.getName());
        assertEquals(expectedCharacter.getOrigin().getName(), resultCharacter.getOrigin().getName());
    }

    @Test
    void should_return_character_when_origin_url_is_empty(){
        Character expectedCharacter=new Character();
        expectedCharacter.setName("Rick");
        expectedCharacter.setOrigin(new Origin());
        expectedCharacter.getOrigin().setName("unknown");

        //GIVEN
        int id=1;
        Character character=CharacterMocks.characterWithOriginURLEmpty();

        when(getCharacterByID.execute(id)).thenReturn(character);

        //WHEN
        Character resultCharacter=getCharacterByIDUseCase.execute(id);

        //THEN
        assertEquals(expectedCharacter.getName(), resultCharacter.getName());
        assertEquals(expectedCharacter.getOrigin().getName(), resultCharacter.getOrigin().getName());
    }

    @Test
    void should_verify_origin_gateway_execute(){

        int id=1;
        String url="www.earth.cl";

        //GIVEN
        Origin origin=new Origin();
        origin.setUrl(url);

        Character character=CharacterMocks.characterComplete();

        Mockito.when(getCharacterByID.execute(id)).thenReturn(character);
        Mockito.when(getOriginByURLGateway.execute(url)).thenReturn(origin);

        //WHEN
        getCharacterByIDUseCase.execute(id);

        //THEN
        verify(getOriginByURLGateway, atLeastOnce()).execute(url);
    }

    @Test
    void should_verify_origin_gateway_not_execute(){

        int id=1;
        //GIVEN
        Character character=CharacterMocks.characterWithOriginURLEmpty();

        Mockito.when(getCharacterByID.execute(id)).thenReturn(character);

        //WHEN
        getCharacterByIDUseCase.execute(id);

        //THEN
        verify(getOriginByURLGateway, never()).execute("");
    }
*/
}