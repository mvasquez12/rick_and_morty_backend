package cl.mobdev.trainee.rickandmorty.gateway;

import cl.mobdev.trainee.rickandmorty.domain.exception.GatewayException;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.GetOriginByURLGateway;
import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.mapper.OriginMapper;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientOrigin;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GetOriginByURLGatewayTest {


    private GetOriginByURLGateway getOriginByURLGateway;
    @Mock
    private RestTemplate restTemplate;
    @Mock
    private OriginMapper originMapper;

    private String url="www.url.cl";

    @BeforeEach
    void setUp(){
        getOriginByURLGateway=new GetOriginByURLGateway(restTemplate, originMapper);
    }

    @Test
    void should_return_origin_when_call_api() {
        Origin originExpected=new Origin();
        originExpected.setName("Earth");

        //GIVEN
        ResponseEntity<ClientOrigin> origin= new ResponseEntity<>(new ClientOrigin(),HttpStatus.OK);
        origin.getBody().setName("Earth");

        Origin origin1=new Origin();
        origin1.setName("Earth");

        Mockito.when(restTemplate.getForEntity(url, ClientOrigin.class)).thenReturn(origin);
        Mockito.when(originMapper.clientOriginToOrigin(origin.getBody())).thenReturn(origin1);

        //WHEN
        Origin result=getOriginByURLGateway.execute(url);

        //THEN
        assertEquals(originExpected.getName(), result.getName());
    }

    @Test
    void should_throw_exception_when_api_error(){
        String messageExpected="Origin Gateway Error";

        //GIVEN
        Mockito.when(restTemplate.getForEntity(url+1, ClientOrigin.class)).thenThrow(RestClientException.class);

        //WHEN
        GatewayException thrown=assertThrows(GatewayException.class, ()->getOriginByURLGateway.execute(url+1));

        //THEN
        assertEquals(messageExpected, thrown.getMessage());
    }
}