package cl.mobdev.trainee.rickandmorty.gateway;

import cl.mobdev.trainee.rickandmorty.domain.exception.CharacterNotFoundException;
import cl.mobdev.trainee.rickandmorty.domain.exception.GatewayException;
import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.GetCharacterByIDGateway;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class GetCharacterByIDGatewayTest {

    private GetCharacterByIDGateway getCharacterByIDGateway;

    @Mock
    private RestTemplate restTemplate;

    private String url="www.api.cl";

    @BeforeEach
    void setUp(){
        getCharacterByIDGateway=new GetCharacterByIDGateway(url, restTemplate);
    }

    @Test
    void should_return_character_when_call_api(){
        ClientCharacter clientCharacterExpected =new ClientCharacter();
        clientCharacterExpected.setName("Rick");
        clientCharacterExpected.setSpecies("Human");

        //GIVEN
        ResponseEntity<ClientCharacter> character=new ResponseEntity<>(new ClientCharacter(),HttpStatus.OK);
        character.getBody().setName("Rick");
        character.getBody().setSpecies("Human");

        Mockito.when(restTemplate.getForEntity(url+1, ClientCharacter.class)).thenReturn(character);

        //WHEN
        Character result= getCharacterByIDGateway.execute(1);

        //THEN
        assertEquals(clientCharacterExpected.getName(), result.getName());
        assertEquals(clientCharacterExpected.getSpecies(), result.getSpecies());
    }

    @Test
    void should_throw_exception_when_character_not_found_in_api(){
        String messageExpected="Character not found in API";

        //GIVEN
        Mockito.when(restTemplate.getForEntity(url+2, ClientCharacter.class)).thenThrow(HttpClientErrorException.class);

        //WHEN
        CharacterNotFoundException thrown=assertThrows(CharacterNotFoundException.class, ()->getCharacterByIDGateway.execute(2));

        //THEN
        assertEquals(messageExpected, thrown.getMessage());
    }

    @Test
    void should_throw_exception_when_api_error(){
        String messageExpected="Character Gateway Error";

        //GIVEN
        Mockito.when(restTemplate.getForEntity(url+1, ClientCharacter.class)).thenThrow(RestClientException.class);

        //WHEN
        GatewayException thrown=assertThrows(GatewayException.class, ()->getCharacterByIDGateway.execute(1));

        //THEN
        assertEquals(messageExpected, thrown.getMessage());
    }

    @Test
    void should_verify_restTemplate_call_once_time(){
        int wantedNumberInvocations=1;
        int id= 1;

        //GIVEN
        Mockito.when(restTemplate.getForEntity(url+id, ClientCharacter.class)).thenReturn(new ResponseEntity<>(new ClientCharacter(), HttpStatus.OK));

        //WHEN
        getCharacterByIDGateway.execute(id);

        //THEN
        verify(restTemplate, Mockito.atLeastOnce()).getForEntity(url+id, ClientCharacter.class);
    }
}