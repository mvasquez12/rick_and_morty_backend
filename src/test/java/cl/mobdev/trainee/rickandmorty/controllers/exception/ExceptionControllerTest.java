package cl.mobdev.trainee.rickandmorty.controllers.exception;

import cl.mobdev.trainee.rickandmorty.domain.exception.CharacterNotFoundException;
import cl.mobdev.trainee.rickandmorty.domain.exception.GatewayException;
import cl.mobdev.trainee.rickandmorty.infrastructure.controller.exception.ExceptionController;
import cl.mobdev.trainee.rickandmorty.domain.model.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

class ExceptionControllerTest {

    private ExceptionController exceptionController;

    @BeforeEach
    void setUp(){
        exceptionController=new ExceptionController();
    }

    @Test
    void should_return_status_400_when_numberFormatException_throw(){
        int statusExpected=400;

        ResponseEntity<Response> thrown=exceptionController.numberFormatException(new NumberFormatException());

        assertEquals(statusExpected, thrown.getStatusCodeValue());
    }
    @Test
    void should_return_message_when_numberFormatException_throw(){
        String messageExpected="Error Number Format Exception - Error al convertir el ID";

        ResponseEntity<Response> thrown=exceptionController.numberFormatException(new NumberFormatException());

        assertEquals(messageExpected, thrown.getBody().getMessage());
    }

    @Test
    void should_return_status_404_when_characterNotFound_throw(){
        int statusExpected=404;

        ResponseEntity<Response> thrown=exceptionController.characterNotFound(new CharacterNotFoundException(""));

        assertEquals(statusExpected, thrown.getStatusCodeValue());
    }
    @Test
    void should_return_message_when_characterNotFound_throw(){
        String messageExpected="Error Character Not Found - El personaje no se encuentra en la api";

        ResponseEntity<Response> thrown=exceptionController.characterNotFound(new CharacterNotFoundException(""));

        assertEquals(messageExpected, thrown.getBody().getMessage());
    }

    @Test
    void should_return_status_400_when_gatewayException_throw(){
        int statusExpected=502;

        ResponseEntity<Response> thrown=exceptionController.gatewayException(new GatewayException(""));

        assertEquals(statusExpected, thrown.getStatusCodeValue());
    }
    @Test
    void should_return_message_when_gatewayException_throw(){
        String messageExpected="Error Gateway Exception - Error en la conexion con la api";

        ResponseEntity<Response> thrown=exceptionController.gatewayException(new GatewayException(""));

        assertEquals(messageExpected, thrown.getBody().getMessage());
    }

}