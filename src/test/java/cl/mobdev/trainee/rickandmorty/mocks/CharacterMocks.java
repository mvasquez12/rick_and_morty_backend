package cl.mobdev.trainee.rickandmorty.mocks;

import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import cl.mobdev.trainee.rickandmorty.domain.model.Origin;

public class CharacterMocks {

    static public Character characterComplete(){
        Character character =new Character();

        Origin origin=new Origin();
        origin.setUrl("www.earth.cl");

        character.setId(1);
        character.setName("Rick");
        character.setStatus("Alive");
        character.setType("Human");
        character.setEpisode_counts(0);
        character.setOrigin(origin);

        return character;
    }

    static public Character characterWithOriginURLEmpty(){
        Character character =new Character();

        Origin origin=new Origin();
        origin.setUrl("");

        character.setId(1);
        character.setName("Rick");
        character.setStatus("Alive");
        character.setType("Human");
        character.setEpisode_counts(0);
        character.setOrigin(origin);

        return character;
    }

    static public Character characterWithOriginNull(){
        Character character =new Character();

        character.setId(1);
        character.setName("Rick");
        character.setStatus("Alive");
        character.setType("Human");
        character.setEpisode_counts(0);

        return character;
    }
}
