package cl.mobdev.trainee.rickandmorty.service.toTest;

import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.CharacterIsFromEarthUseCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;


public class CharacterIsFromEarthTest {

    private CharacterIsFromEarthUseCase characterIsFromEarthUseCase;

    private Origin origin;
    private ClientCharacter clientCharacter;

    @BeforeEach
    void setUp(){
        characterIsFromEarthUseCase=new CharacterIsFromEarthUseCase();

    }

    @Test
    void should_be_true_if_character_is_from_earth() {
        boolean expected = true;

        //GIVEN
        clientCharacter =new ClientCharacter();
        origin=new Origin();

        origin.setName("Earth");
        clientCharacter.setOrigin(origin);

        //THEN
        boolean response=characterIsFromEarthUseCase.execute(clientCharacter);

        //WHEN
        Assertions.assertEquals(expected, response);
    }

    @Test
    void should_be_false_if_character_is_from_another_planet(){
        boolean expected=false;

        //GIVEN
        clientCharacter =new ClientCharacter();
        origin=new Origin();

        origin.setName("Jupiter");
        clientCharacter.setOrigin(origin);

        //WHEN
        boolean response=characterIsFromEarthUseCase.execute(clientCharacter);

        //THEN
        Assertions.assertEquals(expected, response);
    }

    @Test
    void should_be_false_if_origin_is_null(){

        //GIVEN
        clientCharacter =new ClientCharacter();
        clientCharacter.setName("Rick");

        //WHEN
        boolean result= characterIsFromEarthUseCase.execute(clientCharacter);

        //THEN
        assertFalse(result);
    }

    @Test
    void should_be_false_if_origin_name_is_null(){

        //GIVEN
        clientCharacter =new ClientCharacter();
        clientCharacter.setName("Rick");
        clientCharacter.setOrigin(new Origin());


        //WHEN
        boolean result= characterIsFromEarthUseCase.execute(clientCharacter);

        //THEN
        assertFalse(result);
    }
}
