package cl.mobdev.trainee.rickandmorty.service.toTest;

import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.CharacterListUseCase;
import cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.interfaces.ICharacterListInterface;
import cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.components.MartianTranslate;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class CharacterListTest {

    /*
    Tendrá un método que no recibe parámetros
    usara un Gateway que trae un listado de characters
    y retornara ese listado de characteres ordenado por nombre,
    pero a los characteres que sean del planeta "Mars",
    se les reescribirá el atributo "type" usando lenguaje marciano
    usando como dato de entrada el nombre.
    Algoritmo lenguaje marciano:
    Dado un texto cada vocal es reemplazada por un numero; a(1), e(2), i(3), o(4), u(5)
    todos los caracteres son minúsculas y sin espacios.
    Por ejemplo, nombre "Hola Mundo" --> "h4l1m5nd4"
     */

    @Mock
    private ICharacterListInterface iCharacterListInterface;
    @Mock
    private MartianTranslate martianTranslate;
    private CharacterListUseCase characterListUseCase;

    @BeforeEach
    void setUp(){
        characterListUseCase=new CharacterListUseCase(iCharacterListInterface, martianTranslate);
    }

    @Test
    void should_return_empty_list_when_list_of_characters_is_empty(){
        ArrayList<ClientCharacter> expectedList=new ArrayList();

        //GIVEN
        ArrayList<ClientCharacter> listClientCharacters =new ArrayList<>();
        Mockito.when(iCharacterListInterface.execute()).thenReturn(listClientCharacters);

        //THEN
        ArrayList<ClientCharacter> responseList=characterListUseCase.execute();

        //WHEN
        assertEquals(expectedList, responseList);
    }

    @Test
    void should_return_sort_list_of_characters() {
        ArrayList<ClientCharacter> expectedOrder = new ArrayList<>();

        //GIVEN
        ClientCharacter clientCharacter = new ClientCharacter();
        ClientCharacter clientCharacter2 = new ClientCharacter();
        ClientCharacter clientCharacter3 = new ClientCharacter();
        clientCharacter.setName("Angel");
        clientCharacter2.setName("Morty");
        clientCharacter3.setName("Rick");

        expectedOrder.add(clientCharacter);
        expectedOrder.add(clientCharacter2);
        expectedOrder.add(clientCharacter3);

        ArrayList<ClientCharacter> listClientCharacters =new ArrayList<>();
        listClientCharacters.add(clientCharacter3);
        listClientCharacters.add(clientCharacter);
        listClientCharacters.add(clientCharacter2);

        Mockito.when(iCharacterListInterface.execute()).thenReturn(listClientCharacters);

        //WHEN
        ArrayList<ClientCharacter> response=characterListUseCase.execute();

        //THEN
        assertEquals(expectedOrder, response);

        /*for(int i=0; i<expectedOrder.size();i++)
        {
            assertEquals(expectedOrder.get(i).getName(), response.get(i).getName());
        }*/
    }

    @Test
    void should_return_type_when_origin_is_null() {
        String expectedType="Human";

        //GIVEN
        ClientCharacter clientCharacter =new ClientCharacter();
        clientCharacter.setName("Rick");
        clientCharacter.setType("Human");

        ArrayList<ClientCharacter> listClientCharacters =new ArrayList<>();
        listClientCharacters.add(clientCharacter);
        Mockito.when(iCharacterListInterface.execute()).thenReturn(listClientCharacters);
        //WHEN
        ArrayList<ClientCharacter> response=characterListUseCase.execute();
        //THEN
        assertEquals(expectedType, response.get(0).getType());
    }

    @Test
    void should_return_type_when_origin_name_is_null() {
        String expectedType="Human";

        //GIVEN
        ClientCharacter clientCharacter =new ClientCharacter();
        clientCharacter.setName("Rick");
        clientCharacter.setType("Human");
        clientCharacter.setOrigin(new Origin());

        ArrayList<ClientCharacter> listClientCharacters =new ArrayList<>();
        listClientCharacters.add(clientCharacter);
        Mockito.when(iCharacterListInterface.execute()).thenReturn(listClientCharacters);
        //WHEN
        ArrayList<ClientCharacter> response=characterListUseCase.execute();
        //THEN
        assertEquals(expectedType, response.get(0).getType());
    }

    @Test
    void should_return_type_with_martian_language(){
        String expectedType="1l32n";

        //GIVEN
        ClientCharacter clientCharacter =new ClientCharacter();
        clientCharacter.setName("Alien");
        clientCharacter.setOrigin(new Origin());
        clientCharacter.getOrigin().setName("Mars");

        ArrayList<ClientCharacter> listClientCharacters =new ArrayList<>();
        listClientCharacters.add(clientCharacter);

        Mockito.when(iCharacterListInterface.execute()).thenReturn(listClientCharacters);
        Mockito.when(martianTranslate.execute(clientCharacter.getName())).thenReturn(expectedType);

        //WHEN
        ArrayList<ClientCharacter> response=characterListUseCase.execute();

        //THEN
        assertEquals(expectedType, response.get(0).getType());
    }

    @Test
    void should_return_type_when_character_not_martian(){
        String expectedType="Human";

        //GIVEN
        ClientCharacter clientCharacter =new ClientCharacter();
        clientCharacter.setName("Human");
        clientCharacter.setOrigin(new Origin());
        clientCharacter.getOrigin().setName("Earth");
        clientCharacter.setType("Human");

        ArrayList<ClientCharacter> listClientCharacters =new ArrayList<>();
        listClientCharacters.add(clientCharacter);

        Mockito.when(iCharacterListInterface.execute()).thenReturn(listClientCharacters);

        //WHEN
        ArrayList<ClientCharacter> response=characterListUseCase.execute();

        //THEN
        assertEquals(expectedType, response.get(0).getType());
    }

}
