package cl.mobdev.trainee.rickandmorty.service.toTest;

import cl.mobdev.trainee.rickandmorty.domain.exception.LocationException;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.Location;
import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.CharacterLiveInOriginUseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CharacterLiveInOriginTest {

    /*
    Tendrá un método que dado un Character,
    retorna un true si donde vive es el mismo lugar de su origen
    y false en cualquier otro caso.
    Lanzara un LocationException si su origen es desconocido
    Lanzara un LocationException si su locación es desconocido
    Lanzara un LocationException si su origen y locación son desconocido
    */

    private CharacterLiveInOriginUseCase characterLiveInOriginUseCase;

    private ClientCharacter clientCharacter;
    private Origin origin;
    private Location location;

    @BeforeEach
    void setUp(){
        characterLiveInOriginUseCase=new CharacterLiveInOriginUseCase();
    }

    @Test
    void should_return_true_when_character_live_in_origin(){
        boolean expected=true;

        //GIVEN
        clientCharacter =new ClientCharacter();
        origin=new Origin();
        location=new Location();

        origin.setName("Earth");
        location.setName("Earth");
        clientCharacter.setLocation(location);
        clientCharacter.setOrigin(origin);
        //WHEN
        boolean response= characterLiveInOriginUseCase.execute(clientCharacter);
        //THEN
        assertEquals(expected, response);
    }

    @Test
    void should_return_false_when_character_not_live_in_origin(){
        boolean expected=false;

        //GIVEN
        clientCharacter =new ClientCharacter();
        origin=new Origin();
        location=new Location();

        origin.setName("Mars");
        location.setName("Earth");
        clientCharacter.setLocation(location);
        clientCharacter.setOrigin(origin);
        //WHEN
        boolean response= characterLiveInOriginUseCase.execute(clientCharacter);
        //THEN
        assertEquals(expected, response);
    }

    @Test
    void should_throw_exception_when_origin_is_unknown(){
        LocationException expectedException=new LocationException("");
        String expectedMessage="Origin unknown";
        //GIVEN
        clientCharacter =new ClientCharacter();
        origin=new Origin();
        location=new Location();

        origin.setName("unknown");
        location.setName("Earth");
        clientCharacter.setOrigin(origin);
        clientCharacter.setLocation(location);
        //WHEN
        LocationException thrown=assertThrows(expectedException.getClass(), ()-> characterLiveInOriginUseCase.execute(clientCharacter));
        //THEN
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    void should_throw_exception_when_location_is_unknown(){
        LocationException expectedException=new LocationException("");
        String expectedMessage="Location unknown";
        //GIVEN
        clientCharacter =new ClientCharacter();
        origin=new Origin();
        location=new Location();

        origin.setName("Earth");
        location.setName("unknown");
        clientCharacter.setOrigin(origin);
        clientCharacter.setLocation(location);
        //WHEN
        LocationException thrown=assertThrows(expectedException.getClass(), ()-> characterLiveInOriginUseCase.execute(clientCharacter));
        //THEN
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    void should_throw_exception_when_location_and_origin_is_unknown(){
        LocationException expectedException=new LocationException("");
        String expectedMessage="Origin and location unknown";
        //GIVEN
        clientCharacter =new ClientCharacter();
        origin=new Origin();
        location=new Location();

        origin.setName("unknown");
        location.setName("unknown");
        clientCharacter.setOrigin(origin);
        clientCharacter.setLocation(location);
        //WHEN
        LocationException thrown=assertThrows(expectedException.getClass(), ()-> characterLiveInOriginUseCase.execute(clientCharacter));
        //THEN
        assertEquals(expectedMessage, thrown.getMessage());
    }

    @Test
    void should_throw_exception_when_location_and_origin_is_null(){
        LocationException expectedException=new LocationException("");
        String expectedMessage="Origin and location unknown";

        //GIVEN
        clientCharacter =new ClientCharacter();
        clientCharacter.setName("Rick");

        //WHEN
        LocationException thrown=assertThrows(expectedException.getClass(), ()->characterLiveInOriginUseCase.execute(clientCharacter));
        //THEN
        assertEquals(expectedMessage, thrown.getMessage());
    }
}
