package cl.mobdev.trainee.rickandmorty.infrastructure.gateway.mapper;

import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientOrigin;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OriginMapperTest {

    private OriginMapper originMapper;

    @BeforeEach
    void setUp(){
        originMapper=new OriginMapper();
    }

    @Test
    void should_return_origin_when_call_originMapper(){
        Origin originExpected=new Origin();
        originExpected.setName("Earth");

        //GIVEN
        ClientOrigin clientOrigin=new ClientOrigin();
        clientOrigin.setName("Earth");
        //WHEN
        Origin response=originMapper.clientOriginToOrigin(clientOrigin);
        //THEN
        assertEquals(originExpected.getName(), response.getName());
    }

}