package cl.mobdev.trainee.rickandmorty.infrastructure.gateway.mapper;

import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CharacterMapperTest {

    @Test
    void should_return_characterDTO_when_call_mapper_with_null_episode_array(){
        Character characterExpected=new Character();
        characterExpected.setName("Rick");

        //GIVEN
        ClientCharacter clientCharacter =new ClientCharacter();
        clientCharacter.setName("Rick");

        //WHEN
        Character response=CharacterMapper.INSTANCE.clientCharacterToCharacter(clientCharacter);

        //THEN
        assertEquals(characterExpected.getName(), response.getName());
    }

    @Test
    void should_return_character_when_call_mapper_with_array_episode_not_null(){
        Character characterExpected=new Character();
        characterExpected.setName("Rick");

        //GIVEN
        ClientCharacter clientCharacter =new ClientCharacter();
        clientCharacter.setName("Rick");
        clientCharacter.setEpisode(new ArrayList());

        //WHEN
        Character response=CharacterMapper.INSTANCE.clientCharacterToCharacter(clientCharacter);

        //THEN
        assertEquals(characterExpected.getName(), response.getName());

    }

}