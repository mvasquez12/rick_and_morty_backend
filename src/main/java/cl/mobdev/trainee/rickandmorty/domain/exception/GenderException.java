package cl.mobdev.trainee.rickandmorty.domain.exception;

public class GenderException extends RuntimeException{
    public GenderException(String message) {
        super(message);
    }
}
