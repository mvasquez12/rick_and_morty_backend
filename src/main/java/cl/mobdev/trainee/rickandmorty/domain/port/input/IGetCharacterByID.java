package cl.mobdev.trainee.rickandmorty.domain.port.input;

import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import com.google.firebase.auth.FirebaseAuthException;

import java.io.IOException;


public interface IGetCharacterByID {
     Character execute(int id) throws IOException, FirebaseAuthException;
}
