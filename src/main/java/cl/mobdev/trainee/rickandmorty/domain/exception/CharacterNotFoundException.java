package cl.mobdev.trainee.rickandmorty.domain.exception;

public class CharacterNotFoundException extends RuntimeException{
    public CharacterNotFoundException(String message) {
        super(message);
    }
}
