package cl.mobdev.trainee.rickandmorty.domain.port.output;

import cl.mobdev.trainee.rickandmorty.domain.model.Origin;

public interface IGetOriginByURLGateway {
    Origin execute(String uri);
}
