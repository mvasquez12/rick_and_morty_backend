package cl.mobdev.trainee.rickandmorty.domain.model;

public class Character {
    private int id;
    private String name;
    private String status;
    private String species;
    private String type;
    private int episode_counts;

    private int prueba;
    private Origin origin;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getEpisode_counts() {
        return episode_counts;
    }

    public void setEpisode_counts(int episode_counts) {
        this.episode_counts = episode_counts;
    }

    public Origin getOrigin() {
        return origin;
    }

    public void setOrigin(Origin origin) {
        this.origin = origin;
    }

    public int getPrueba() {
        return prueba;
    }

    public void setPrueba(int prueba) {
        this.prueba = prueba;
    }
}
