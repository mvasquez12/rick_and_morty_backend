package cl.mobdev.trainee.rickandmorty.domain.exception;

public class GatewayException extends RuntimeException{
    public GatewayException(String message) {
        super(message);
    }
}
