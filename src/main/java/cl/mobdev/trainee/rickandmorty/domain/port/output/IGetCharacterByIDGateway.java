package cl.mobdev.trainee.rickandmorty.domain.port.output;

import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import org.springframework.stereotype.Repository;

@Repository
public interface IGetCharacterByIDGateway {
    Character execute(int id);
}
