package cl.mobdev.trainee.rickandmorty.domain.usecase;

import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import cl.mobdev.trainee.rickandmorty.domain.port.output.IGetCharacterByIDGateway;
import cl.mobdev.trainee.rickandmorty.domain.port.output.IGetOriginByURLGateway;
import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import cl.mobdev.trainee.rickandmorty.domain.port.input.IGetCharacterByID;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import org.apache.catalina.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class GetCharacterByIDUseCase implements IGetCharacterByID {
    private final IGetCharacterByIDGateway getCharacterByID;
    private final IGetOriginByURLGateway getOriginByURL;

    public GetCharacterByIDUseCase(IGetCharacterByIDGateway getCharacterByID, IGetOriginByURLGateway getOriginByURL) {
        this.getCharacterByID = getCharacterByID;
        this.getOriginByURL = getOriginByURL;
    }

    @Override
    public Character execute(int id) {


        InputStream serviceAccount = getClass().getClassLoader().getResourceAsStream("padok_api_key.json");

        FirebaseOptions options = null;
        try {
            options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .build();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        FirebaseApp.initializeApp(options);

        UserRecord userRecord= null;
        try {
            userRecord = FirebaseAuth.getInstance().getUser("Ni6ZSaTzxRcfsNMYpDs4o2tVsT92");
        } catch (FirebaseAuthException e) {
            throw new RuntimeException(e);
        }
        System.out.println(userRecord.getUid());
        System.out.println(userRecord.getEmail());
        System.out.println(userRecord.getDisplayName());
        System.out.println(userRecord.getPhotoUrl());


        Character character = getCharacterByID.execute(id);

        if (character.getOrigin() == null || character.getOrigin().getUrl().trim().equals("")) {
            character.setOrigin(new Origin());
            character.getOrigin().setName("unknown");
            character.getOrigin().setUrl("");
            character.getOrigin().setResidents(new ArrayList());
            character.getOrigin().setDimension("");
        } else {
            character.setOrigin(getOriginByURL.execute(character.getOrigin().getUrl()));
        }

        return character;
    }
}
