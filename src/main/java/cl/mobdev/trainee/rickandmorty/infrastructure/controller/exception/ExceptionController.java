package cl.mobdev.trainee.rickandmorty.infrastructure.controller.exception;

import cl.mobdev.trainee.rickandmorty.domain.exception.CharacterNotFoundException;
import cl.mobdev.trainee.rickandmorty.domain.exception.GatewayException;
import cl.mobdev.trainee.rickandmorty.domain.model.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


@ControllerAdvice
public class ExceptionController {
    private Response response;

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<Response> numberFormatException(NumberFormatException ex){
        response=new Response();
        response.setCode(400);
        response.setData(ex.getMessage());
        response.setMessage("Error Number Format Exception - Error al convertir el ID");
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CharacterNotFoundException.class)
    public ResponseEntity<Response> characterNotFound(CharacterNotFoundException ex){
        response=new Response();
        response.setCode(404);
        response.setData(ex.getMessage());
        response.setMessage("Error Character Not Found - El personaje no se encuentra en la api");
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(GatewayException.class)
    public ResponseEntity<Response> gatewayException(GatewayException ex){
        response=new Response();
        response.setCode(502);
        response.setData(ex.getMessage());
        response.setMessage("Error Gateway Exception - Error en la conexion con la api");
        return new ResponseEntity<>(response, HttpStatus.BAD_GATEWAY);
    }

    /*

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<Response> runtimeError(RuntimeException ex){
        response=new Response();
        response.setCode(500);
        response.setData(ex.getMessage());
        response.setMessage("Runtime Error");
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

    }*/
}
