package cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest;

import cl.mobdev.trainee.rickandmorty.domain.exception.GenderException;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.interfaces.IIsFemaleInterface;


public class IsFemaleUseCase {

    private String genderRequired = "Female";
    private final IIsFemaleInterface iIsFemaleInterface;

    public IsFemaleUseCase(IIsFemaleInterface iIsFemaleInterface) {
        this.iIsFemaleInterface = iIsFemaleInterface;
    }

    public ClientCharacter execute(int id) {
        String characterGender = iIsFemaleInterface.getFemaleCharacter(1).getGender();
        if (characterGender == null) {
            throw new GenderException("El character no es mujer");
        } else {
            if (genderRequired.equals(characterGender)) {
                return iIsFemaleInterface.getFemaleCharacter(id);
            } else {
                throw new GenderException("El character no es mujer");
            }
        }
    }
}
