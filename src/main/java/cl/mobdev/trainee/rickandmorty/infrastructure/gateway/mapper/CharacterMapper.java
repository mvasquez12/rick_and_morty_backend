package cl.mobdev.trainee.rickandmorty.infrastructure.gateway.mapper;

import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import org.mapstruct.BeforeMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CharacterMapper {
    CharacterMapper INSTANCE = Mappers.getMapper( CharacterMapper.class );

    @BeforeMapping
    default void setEpisodeCount(ClientCharacter clientCharacter, @MappingTarget Character character) {
        if(clientCharacter.getEpisode()!=null)
        {
            character.setEpisode_counts(clientCharacter.getEpisode().size());
        }else{
            character.setEpisode_counts(0);
        }
    }
    Character clientCharacterToCharacter(ClientCharacter clientCharacter);

}
