package cl.mobdev.trainee.rickandmorty.infrastructure.config;

import cl.mobdev.trainee.rickandmorty.domain.port.input.IGetCharacterByID;
import cl.mobdev.trainee.rickandmorty.domain.port.output.IGetCharacterByIDGateway;
import cl.mobdev.trainee.rickandmorty.domain.port.output.IGetOriginByURLGateway;
import cl.mobdev.trainee.rickandmorty.domain.usecase.GetCharacterByIDUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class config {
    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    public GetCharacterByIDUseCase getCharacterByIDUseCase(IGetCharacterByIDGateway iGetCharacterByIDGateway, IGetOriginByURLGateway iGetOriginByURLGateway){
        return new GetCharacterByIDUseCase(iGetCharacterByIDGateway, iGetOriginByURLGateway);
    }
}
