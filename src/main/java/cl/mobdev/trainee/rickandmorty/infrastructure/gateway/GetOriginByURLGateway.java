package cl.mobdev.trainee.rickandmorty.infrastructure.gateway;

import cl.mobdev.trainee.rickandmorty.domain.exception.GatewayException;
import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import cl.mobdev.trainee.rickandmorty.domain.port.output.IGetOriginByURLGateway;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.mapper.OriginMapper;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientOrigin;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public class GetOriginByURLGateway implements IGetOriginByURLGateway {

    private final RestTemplate restTemplate;
    private final OriginMapper originMapper;

    public GetOriginByURLGateway(RestTemplate restTemplate, OriginMapper originMapper) {
        this.restTemplate = restTemplate;
        this.originMapper = originMapper;
    }

    @Override
    public Origin execute(String uri) {
        ClientOrigin origin=null;

        try {
            ResponseEntity<ClientOrigin> response= restTemplate.getForEntity(uri, ClientOrigin.class);
            origin=response.getBody();
        }catch (RestClientException ex) {
            throw new GatewayException("Origin Gateway Error");
        }

        return originMapper.clientOriginToOrigin(origin);
    }
}
