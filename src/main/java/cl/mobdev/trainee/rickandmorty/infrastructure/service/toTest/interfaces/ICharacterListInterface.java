package cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.interfaces;

import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;

import java.util.ArrayList;


public interface ICharacterListInterface {

    ArrayList<ClientCharacter> execute();
}
