package cl.mobdev.trainee.rickandmorty.infrastructure.controller;

import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import cl.mobdev.trainee.rickandmorty.domain.port.input.IGetCharacterByID;
import com.google.firebase.auth.FirebaseAuthException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;


@RestController
@RequestMapping(value="/api/character")
public class CharacterController {

    private final IGetCharacterByID iGetCharacterByID;

    public CharacterController(IGetCharacterByID iGetCharacterByID) {
        this.iGetCharacterByID = iGetCharacterByID;
    }

    @GetMapping(value="/{id}")
    public ResponseEntity<Character> getData(@PathVariable int id) {
        try {
            return new ResponseEntity<>(iGetCharacterByID.execute(id), HttpStatus.OK);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (FirebaseAuthException e) {
            throw new RuntimeException(e);
        }
    }

}
