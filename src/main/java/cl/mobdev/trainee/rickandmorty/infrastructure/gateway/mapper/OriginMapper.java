package cl.mobdev.trainee.rickandmorty.infrastructure.gateway.mapper;

import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientOrigin;
import org.springframework.stereotype.Component;

@Component
public class OriginMapper {

    public Origin clientOriginToOrigin(ClientOrigin clientOrigin){
        Origin origin=new Origin();
        origin.setName(clientOrigin.getName());
        origin.setUrl(clientOrigin.getUrl());
        origin.setDimension(clientOrigin.getDimension());
        origin.setResidents(clientOrigin.getResidents());

        return origin;
    }

}
