package cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest;

import cl.mobdev.trainee.rickandmorty.domain.exception.LocationException;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;


public class CharacterLiveInOriginUseCase {

    private String unknown = "unknown";


    public boolean execute(ClientCharacter clientCharacter) {

        if (clientCharacter.getLocation()==null && clientCharacter.getOrigin()==null || unknown.equals(clientCharacter.getLocation().getName()) && unknown.equals(clientCharacter.getOrigin().getName())) {
            throw new LocationException("Origin and location unknown");
        } else {
            if (clientCharacter.getLocation().getName()==null || unknown.equals(clientCharacter.getOrigin().getName())) {
                throw new LocationException("Origin unknown");
            } else {
                if (clientCharacter.getLocation()==null || unknown.equals(clientCharacter.getLocation().getName())) {
                    throw new LocationException("Location unknown");
                } else {
                    if (clientCharacter.getOrigin().getName().equals(clientCharacter.getLocation().getName())) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

    }
}
