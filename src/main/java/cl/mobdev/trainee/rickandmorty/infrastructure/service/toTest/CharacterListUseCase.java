package cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest;

import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import cl.mobdev.trainee.rickandmorty.domain.model.Origin;
import cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.components.MartianTranslate;
import cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.interfaces.ICharacterListInterface;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class CharacterListUseCase implements ICharacterListInterface {

    private String MARS = "Mars";
    private final ICharacterListInterface characterListInterface;
    private final MartianTranslate martianTranslate;

    public CharacterListUseCase(ICharacterListInterface characterListInterface, MartianTranslate martianTranslate) {
        this.characterListInterface = characterListInterface;
        this.martianTranslate = martianTranslate;
    }

    @Override
    public ArrayList<ClientCharacter> execute() {
        ArrayList<ClientCharacter> listClientCharacters = characterListInterface.execute();

        if (!listClientCharacters.isEmpty()) {
            listClientCharacters.stream().forEach((character) -> {
                if (character.getOrigin() != null && character.getOrigin().getName() != null) {
                    if (MARS.equals(character.getOrigin().getName())) {
                        character.setType(martianTranslate.execute(character.getName()));
                    }
                }else{
                    character.setOrigin(new Origin());
                    character.getOrigin().setName("unknown");
                }
            });
            //listCharacters.sort((a, b) -> a.getName().compareToIgnoreCase(b.getName()));
            listClientCharacters = listClientCharacters.stream().sorted((a, b) -> a.getName().compareToIgnoreCase(b.getName())).collect(Collectors.toCollection(ArrayList::new));

        } else {
            return listClientCharacters;
        }

        return listClientCharacters;
    }
}
