package cl.mobdev.trainee.rickandmorty.infrastructure.gateway;

import cl.mobdev.trainee.rickandmorty.domain.exception.CharacterNotFoundException;
import cl.mobdev.trainee.rickandmorty.domain.exception.GatewayException;
import cl.mobdev.trainee.rickandmorty.domain.model.Character;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.mapper.CharacterMapper;
import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import cl.mobdev.trainee.rickandmorty.domain.port.output.IGetCharacterByIDGateway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Component
public class GetCharacterByIDGateway implements IGetCharacterByIDGateway {

    private final String url;
    private final RestTemplate restTemplate;

    public GetCharacterByIDGateway(@Value("${url}")String url, RestTemplate restTemplate) {
        this.url = url;
        this.restTemplate = restTemplate;
    }

    @Override
    public Character execute(int id) {
        ClientCharacter clientCharacter =null;
        try {
            ResponseEntity<ClientCharacter> response = restTemplate.getForEntity(url + id, ClientCharacter.class);
            clientCharacter = response.getBody();

        } catch(HttpClientErrorException ex){
            throw new CharacterNotFoundException("Character not found in API");
        } catch (RestClientException ex) {
            throw new GatewayException("Character Gateway Error");
        }

        return CharacterMapper.INSTANCE.clientCharacterToCharacter(clientCharacter);
    }
}
