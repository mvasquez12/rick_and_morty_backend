package cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest;

import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;

import java.util.ArrayList;

public class HowManyAliensAreResident {

    private String ALIEN = "Alien";

    public int execute(ArrayList<ClientCharacter> listClientCharacters) {

        return listClientCharacters.stream()
                .filter(character -> ALIEN.equals(character.getSpecies()))
                .reduce(0,(accumulator, character) -> accumulator+character.getLocation().getResidents().size(), Integer::sum );
    }
}
