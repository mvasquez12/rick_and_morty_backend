package cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest;

import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;
import cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.interfaces.IIsHumanInterface;


public class IsHumanUseCase {

    private final IIsHumanInterface iIsHumanInterface;
    private String humanSpecie = "Human";

    public IsHumanUseCase(IIsHumanInterface iIsHumanInterface) {
        this.iIsHumanInterface = iIsHumanInterface;
    }

    public boolean execute(int id) {
        ClientCharacter clientCharacter = iIsHumanInterface.getCharacterByID(id);
        if (humanSpecie.equals(clientCharacter.getSpecies())) {
            return true;
        } else {
            return false;
        }
    }
}
