package cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest;

import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;


public class CharacterIsFromEarthUseCase {
    public boolean execute(ClientCharacter clientCharacter) {

        if (clientCharacter.getOrigin()==null || clientCharacter.getOrigin().getName()==null) {
            return false;
        } else {

            if (clientCharacter.getOrigin().getName().equals("Earth")) {
                return true;
            } else {
                return false;
            }
        }
    }
}
