package cl.mobdev.trainee.rickandmorty.infrastructure.service.toTest.interfaces;

import cl.mobdev.trainee.rickandmorty.infrastructure.gateway.model.ClientCharacter;

public interface IIsFemaleInterface {
    ClientCharacter getFemaleCharacter(int id);
}
